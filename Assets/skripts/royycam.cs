﻿using UnityEngine;

public class royycam : MonoBehaviour {

    public float speed = 5f;
    private Transform _royycam;

    private void Start() {
        _royycam = GetComponent<Transform>();
    }

    private void Update() {
        _royycam.Rotate(0, speed * Time.deltaTime, 0);
    }

}